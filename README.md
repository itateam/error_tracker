# Error Tracker server

### System requirements:

- Meteor https://www.meteor.com/install
- node && npm

### Local run
- clone this repo
- cd to project root dir
- run ```npm install```
- open ```settings.json``` file in the root of project
- remember credentials in ```load``` section
- run ```meteor run --settings ./settings.json``` in console (prefer to setup meteor run environment in Webstorm IDE if U are Windows user)
- open http://localhost:3000 and login
- on left menu click Создать проект
- create project and copy snippet
- create test html page anywhere and paste error-tracker.min.js from near repo and init snippet after that
- wright some invalid js code =)
- open your test page in browser
- magic =)