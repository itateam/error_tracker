import React from 'react';
import {mount, withOptions} from 'react-mounter';

import MainLayout from './containers/main_layout.js';
import Dashboard from './components/dashboard';
import Settings from './components/settings';
import Project from './containers/project';
import ErrorPage from './containers/error'
import NewProject from './containers/new_project'
import ProjectSettings from './containers/project_settings'

export default function (injectDeps, {FlowRouter}) {
  const MainLayoutCtx = injectDeps(MainLayout);

  FlowRouter.route('/', {
    name: 'dashboard',
    action() {
      mount(MainLayoutCtx, {
        content: () => (<Dashboard />)
      });
    }
  });
  FlowRouter.route('/settings', {
    name: 'settings',
    action() {
      mount(MainLayoutCtx, {
        content: ()=> (<Settings />)
      })
    }
  })
  FlowRouter.route('/projects/new', {
    name: "projectCreate",
    action(params) {
      mount(MainLayoutCtx, {
        content: () => (<NewProject />)
      })
    }
  })
  FlowRouter.route('/projects/:projectId', {
    name: "project",
    action(params) {
      const pId = params.projectId;
      mount(MainLayoutCtx, {
        content: ()=> (<Project projectId={pId}/>)
      })
    }
  })
  FlowRouter.route('/projects/:projectId/settings', {
    name: "projectCreate",
    action(params) {
      const pId = params.projectId;
      mount(MainLayoutCtx, {
        content: () => (<ProjectSettings projectId={pId}/>)
      })
    }
  })
  FlowRouter.route('/projects/:projectId/errors/:errorId', {
    name: "project",
    action(params) {
      const eId = params.errorId;
      mount(MainLayoutCtx, {
        content: ()=> (<ErrorPage errorId={eId}/>)
      })
    }
  })
}
