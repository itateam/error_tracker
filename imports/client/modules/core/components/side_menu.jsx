import  React from 'react'

class SideMenu extends React.Component {
    render() {
        const {projects} = this.props;
        return <aside className="main-sidebar">
            {/* sidebar: style can be found in sidebar.less */}
            <section className="sidebar">
                {/* sidebar menu: : style can be found in sidebar.less */}
                <ul className="sidebar-menu">
                    <li className={this.isActiveRoute('dashboard') ? 'active' : ''}>
                        <a href="/">
                            <i className="fa fa-dashboard" /> <span>Сводка</span>
                        </a>
                    </li>
                    <li className={`tree-view ${this.isActiveRoute('project') ? 'active' : ''}`}>
                        <a href="#" onClick={this.toggleMenuProjectsList.bind(this)}>
                            <i className="fa fa-list" /> <span>Проекты</span>
                            <i className="fa fa-angle-down pull-right"></i>
                        </a>
                        <ul className={`treeview-menu ${this.props.isProjectsListOpen ? 'menu-open':''}`}>
                            {projects && projects.length ? projects.map(project=>(
                                <li className={this.isActiveProject(project._id) ? 'active':''} key={project._id}>
                                    <a href={`/projects/${project._id}`}><i className="fa fa-circle-o"></i> {project.name}</a>
                                </li>
                            )):''}
                            <li className={this.isActiveRoute('project.new')? 'active':''}>
                                <a href="/projects/new"><i className="fa fa-plus-square-o"></i>Создать проект</a>
                            </li>
                        </ul>
                    </li>
                    <li className={this.isActiveRoute('settings') ? 'active' : ''}>
                        <a href="/settings">
                            <i className="fa fa-cog" /> <span>Настройки</span>
                        </a>
                    </li>
                </ul>
            </section>
            {/* /.sidebar */}
        </aside>
    }
    isActiveRoute(route) {
        return this.props.currentRoute == route;
    }
    isActiveProject(id) {
        return this.props.currentProject == id;
    }
    toggleMenuProjectsList(e) {
        e.preventDefault();
        const {toggleMenuProjectsList} = this.props;
        toggleMenuProjectsList();
    }
}

export default SideMenu