import React from 'react';
import Helmet from 'react-helmet'

class Settings extends React.Component {
    render() {
        return <div className="content-wrapper">
            <Helmet
                title="Настройки Error Tracker"
            />
            <section className="content-header">
                <h1>Настройки Error Tracker</h1>
            </section>
            <section className="content">
                <div className="box">
                    <div className="box-header with-border">
                        <h3 className="box-title">Settings</h3>
                    </div>
                    <div className="box-body">content will be</div>
                    <div className="box-footer">here</div>
                </div>
            </section>
        </div>
    }
}

export default Settings;