import React from 'react';
import Helmet from 'react-helmet'

class Dashboard extends React.Component {
    render() {
        return <div className="content-wrapper">
            <Helmet
                title="Сводка"
            />
            <section className="content-header">
                <h1>Сводка</h1>
            </section>
            <section className="content">
                <div className="box">
                    <div className="box-header with-border">
                        <h3 className="box-title">Dashboard</h3>
                    </div>
                    <div className="box-body">content will be</div>
                    <div className="box-footer">here</div>
                </div>
            </section>
        </div>
    }
}

export default Dashboard;