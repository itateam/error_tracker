import React from 'react';
import Helmet from 'react-helmet'
import moment from 'moment'
import {$} from 'meteor/jquery'
import {_} from 'meteor/underscore'

class ErrorPage extends React.Component {
    render() {
        const {error, isReady} = this.props;

        return <div className="content-wrapper">
            <Helmet
                title={error ? `${error.name}: ${error.message}` : 'Ошибка'}
            />
            <section className="content-header">
                <h1>{error ? `${error.name}: ${error.message}` : 'Загрузка...'}</h1>
            </section>
            <section className="content">
                <div className="row">
                    <div className="col-md-7">
                        <div className="box error-info-box">
                            <div className="box-header with-border">
                                <h3 className="box-title">Инфо</h3>
                            </div>
                            <div className="box-body table-responsive no-padding">
                                {!isReady ? <div className="overlay"><i className="fa fa-refresh fa-spin"></i></div> :
                                    (<div>
                                        <table className="table">
                                            <tbody>
                                            <tr>
                                                <td>Тип</td>
                                                <td>{error.type}</td>
                                            </tr>
                                            <tr>
                                                <td>Класс</td>
                                                <td>{error.name}</td>
                                            </tr>
                                            <tr>
                                                <td>Статус</td>
                                                <td>{error.state == 'resolved' ? 'Исправлена' :
                                                    (<div>Не исправлена <a href="#" onClick={this.resolveError.bind(this)}>Исправил</a></div>)}</td>
                                            </tr>
                                            <tr>
                                                <td>Причина</td>
                                                <td>{error.message}</td>
                                            </tr>
                                            <tr>
                                                <td>Кол-во</td>
                                                <td>{error.count}</td>
                                            </tr>
                                            <tr>
                                                <td>Последнее пояление</td>
                                                <td>{moment(error.lastSeen).format('DD.MM.YYYY HH:mm:ss')}</td>
                                            </tr>
                                            <tr>
                                                <td>Страница</td>
                                                <td><a href={error.page} target="_blank">{error.page}</a></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div className="error-stack-wrap">
                                            <h5>Стек вызова</h5>
                                            <pre><code>{this.getErrorStack(error)}</code></pre>
                                        </div>
                                    </div>)
                                }
                            </div>
                        </div>
                    </div>
                    <div className="col-md-5">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="box collapsed-box">
                                    <div className="box-header with-border">
                                        <h3 className="box-title">Браузеры</h3>
                                        <div className="box-tools pull-right">
                                            <button type="button" onClick={this.boxToggle.bind(this)}
                                                    className="btn btn-box-tool" data-widget="collapse"><i
                                                className="fa fa-minus"></i>
                                            </button>
                                        </div>
                                        <span className="error_browser-icons"
                                              dangerouslySetInnerHTML={!error ? {__html:""} : (this.getBrowsersIconsList(error))}></span>
                                    </div>
                                    <div className="box-body table-responsive no-padding">
                                        <table className="table">
                                            <thead>
                                            <tr>
                                                <th>Браузер</th>
                                                <th>Версия</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {!error ? "" : error.browser.map(browser =>
                                                (<tr key={`${browser.name}${browser.version}`}>
                                                    <td>{browser.name}</td>
                                                    <td>{browser.version}</td>
                                                </tr>))}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-12">
                                <div className="box collapsed-box">
                                    <div className="box-header with-border">
                                        <h3 className="box-title">Операционные системы</h3>
                                        <div className="box-tools pull-right">
                                            <button type="button" onClick={this.boxToggle.bind(this)}
                                                    className="btn btn-box-tool" data-widget="collapse"><i
                                                className="fa fa-minus"></i>
                                            </button>
                                        </div>
                                        <span className="error_browser-icons"
                                              dangerouslySetInnerHTML={!error ? {__html:""} : (this.getOsIconsList(error))}></span>
                                    </div>
                                    <div className="box-body table-responsive no-padding">
                                        <table className="table">
                                            <thead>
                                            <tr>
                                                <th>Операционная система</th>
                                                <th>Версия</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {!error ? "" : error.os.map(os =>
                                                (<tr key={`${os}`}>
                                                    <td>{os}</td>
                                                    <td></td>
                                                </tr>))}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-12">
                                <div className="box collapsed-box">
                                    <div className="box-header with-border">
                                        <h3 className="box-title">Разрешения экранов</h3>
                                        <div className="box-tools pull-right">
                                            <button type="button" onClick={this.boxToggle.bind(this)}
                                                    className="btn btn-box-tool" data-widget="collapse"><i
                                                className="fa fa-minus"></i>
                                            </button>
                                        </div>
                                        <span className="error_browser-icons"
                                              dangerouslySetInnerHTML={!error ? {__html:""} : (this.getResIconsList(error))}></span>
                                    </div>
                                    <div className="box-body table-responsive no-padding">
                                        <table className="table">
                                            <thead>
                                            <tr>
                                                <th>Ширина</th>
                                                <th>Высота</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {!error ? "" : error.resolution.map(res =>
                                                (<tr key={`${res.width}${res.height}`}>
                                                    <td>{res.width}</td>
                                                    <td>{res.height}</td>
                                                </tr>))}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div >
    }

    componentDidMount() {
        this.props.openProjectsMenu()
    }

    getErrorStack(error) {
        let stack = '';
        error.stack.map(function (s) {
            stack += `${s.func} @ ${s.url}:${s.line}:${s.column} \n`
        })
        return stack;
    }

    boxToggle(e) {
        e.preventDefault()

        $(e.target).parents('.box').toggleClass('collapsed-box');
    }

    getBrowsersIconsList(error) {
        const browsers = _.uniq(error.browser, function (a) {
            return a.name;
        });
        let icons = "";

        _.each(browsers, function (browser) {
            const name = browser.name.toLowerCase()
            icons += `<i class="fa fa-${name == "ie" ? "edge" : name == "mobile safari" ? "safari": name}"></i>`
        })
        return {__html: icons};
    }
    getOsIconsList(error) {
        const osList = _.uniq(error.os, function (a) {
            return a;
        });
        let icons = "";

        _.each(osList, function (os) {
            icons += `<i class="fa fa-${os.toLowerCase()}"></i>`
        })
        return {__html: icons};
    }
    getResIconsList(error) {
        const devices = _.countBy(error.resolution, function (res) {
            return res.width < 450 ? "mobile": res.width >= 450 && res.width < 1024 ? 'tablet': 'desktop'
        });
        let resIcons = "";

        _.each(devices, function (val, key) {
            resIcons += `<i class="fa fa-${key.toLowerCase()}"></i>`
        })
        return {__html: resIcons};
    }
    resolveError(e) {
        e.preventDefault();

        const {error, resolve} = this.props;

        resolve(error._id);
    }
}

export default ErrorPage;