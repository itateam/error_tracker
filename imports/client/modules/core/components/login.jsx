import React from 'react';


class Login extends React.Component {
    render() {
        return <div className="login-box">
            <div className="login-logo">
                <span><b>Error</b>Tracker</span>
            </div>{/* /.login-logo */}
            <div className="login-box-body">
                <p class="login-box-msg">Введите эл. почту и пароль</p>
                <form onSubmit={this.login.bind(this)}>
                    <div className="form-group has-feedback">
                        <input ref="email" type="email" className="form-control" placeholder="Эл. почта" required/>
                        <span className="glyphicon glyphicon-envelope form-control-feedback" />
                    </div>
                    <div className="form-group has-feedback">
                        <input ref="pwd" type="password" className="form-control" placeholder="Пароль" required/>
                        <span className="glyphicon glyphicon-lock form-control-feedback" />
                    </div>
                    <div className="row">
                        <div className="col-xs-8">
                        </div>{/* /.col */}
                        <div className="col-xs-4">
                            <button type="submit" className="btn btn-primary btn-block btn-flat" disabled={this.props.loggingIn}>Войти</button>
                        </div>{/* /.col */}
                    </div>
                </form>
                <div className="social-auth-links text-center">
                    { /*<a href="#" className="btn btn-block btn-social btn-google btn-flat"><i className="fa fa-google-plus" /> Sign in using Google+</a> */}
                </div>{/* /.social-auth-links */}
                { /*<a href="#">I forgot my password</a><br />*/}
            </div>{/* /.login-box-body */}
        </div>
    }
    login (e) {
        e.preventDefault();

        const {login} = this.props;
        const {email, pwd} = this.refs;

        login(email.value, pwd.value); //TODO deal with login error
    }
}

export default Login