import React from 'react';
import Helmet from 'react-helmet'

class ProjectSettings extends React.Component {
    render() {
        const {isReady, name, host, accessHash} = this.props;
        return <div className="content-wrapper">
            <Helmet
                title={`Настройки проекта ${name}`}
            />
            <section className="content-header">
                <h1>{`Настройки проекта ${name}`}</h1>
            </section>
            <section className="content">
                <div className="box">
                    <form className="form-horizontal" onSubmit={this.changeName.bind(this)}>
                        <div className="box-header with-border">
                            <h3 className="box-title">Инфо</h3>
                            <button type="button" className="btn btn-danger pull-right btn-xs" onClick={this.removeProject.bind(this)}>Удалить проект</button>
                        </div>
                        <div className="box-body">
                            <div className="form-group">
                                <label className="col-md-2 control-label">Название</label>
                                <div className="col-md-6">
                                    {isReady ?
                                    <input className="form-control" type="text" ref="projectName" onKeyUp={this.nameKeyup.bind(this)} defaultValue={name ? name : ""} required/>
                                    : ''}
                                </div>
                            </div>
                            <div className="form-group">
                                <label className="col-md-2 control-label">Сниппет</label>
                                <div className="col-md-10">
                                    <textarea className="form-control" readOnly value={`ErrorTracker.init({host: "${host}", accessHash: "${accessHash}", environment: "production"});`}>
                                    </textarea>
                                </div>
                            </div>
                        </div>
                        <div className="box-footer text-right">
                            <button className="btn btn-success" type="submit" disabled={!!this.props.isSameName}>Сохранить</button>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    }
    nameKeyup(e) {
        const newName = this.refs.projectName.value;
        const {typeProjectName, name} = this.props;
        
        typeProjectName(name, newName);
    }
    changeName(e) {
        e.preventDefault();
        const {id, name, saveProject} = this.props;
        const newName = this.refs.projectName.value;

        if (newName != name) {
            saveProject(id, newName);
        }
    }
    removeProject(e) {
        const {id, removeProject} = this.props;
        
        if (confirm('Вы действительно хотите удалить проект и все ошибки?')) {
            removeProject(id);
        }
    }
    componentDidMount() {
        this.props.openProjectsMenu()
    }
}

export default ProjectSettings;