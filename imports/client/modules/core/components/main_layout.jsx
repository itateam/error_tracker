import React from 'react';
import Header from '../containers/header'
import Login from '../containers/login'
import SideMenu from '../containers/side_menu'
import Helmet from 'react-helmet'

const Layout = ({loggedIn, loggingIn, actions, isMenuCollapsed, isMenuOpen, pageHeader, content = () => null}) => (
    <div
        className={`skin-blue-light sidebar-mini ${isMenuCollapsed ? 'sidebar-collapse': ''} ${isMenuOpen ? 'sidebar-open': ''}`}>

        {loggedIn ? (
            <div className="wrapper">
                <Helmet
                    meta={[
                        {"name": "viewport", "content": "width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"}
                ]}/>
                <Header />
                <SideMenu />
                {content()}
            </div>
        ) : <Login loggingIn={loggingIn}/>}
    </div>
);

export default Layout;
