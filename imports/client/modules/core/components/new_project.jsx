import React from 'react';
import Helmet from 'react-helmet'

class NewProject extends React.Component {
    render() {
        return <div className="content-wrapper">
            <Helmet
                title="Новый проект"
            />
            <section className="content-header">
                <h1>Новый проект</h1>
            </section>
            <section className="content">
                <div className="col-md-6">
                    <div className="box">
                        <form onSubmit={this.submitForm.bind(this)}>
                            <div className="box-header with-border">
                                <h3 className="box-title">Создать проект</h3>
                            </div>
                            <div className="box-body">
                                <div className="form-group">
                                    <label for="projectName">Название проекта</label>
                                    <input id="projectName" type="text" className="form-control" ref="nameInput"
                                           required/>
                                </div>
                            </div>
                            <div className="box-footer text-right">
                                <button type="submit" className="btn btn-success">Создать</button>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>
    }

    submitForm(e) {
        e.preventDefault();

        const {nameInput} = this.refs;
        const {createProject} = this.props;

        createProject(nameInput.value);
    }
}

export default NewProject;