import React from 'react';

class Header extends React.Component {
    render() {
        return <header className="main-header">
            {/* Logo */}
            <a href="/" className="logo">
                {/* mini logo for sidebar mini 50x50 pixels */}
                <span className="logo-mini"><b>E</b>T</span>
                {/* logo for regular state and mobile devices */}
                <span className="logo-lg"><b>Error</b>Tracker</span>
            </a>
            {/* Header Navbar: style can be found in header.less */}
            <nav className="navbar navbar-static-top" role="navigation">
                {/* Sidebar toggle button*/}
                <a href="#" className="sidebar-toggle" data-toggle="offcanvas" role="button" onClick={this.toggleMenu.bind(this)}>
                    <span className="sr-only">Toggle navigation</span>
                    <span className="icon-bar"/>
                    <span className="icon-bar"/>
                    <span className="icon-bar"/>
                </a>
                <div className="navbar-custom-menu">
                    <ul className="nav navbar-nav">
                        <li className="dropdown user user-menu">
                            <a href="#" onClick={this.logout.bind(this)}>
                                <span>Выйти</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
    }
    logout(e) {
        e.preventDefault();

        const {logout} = this.props;

        logout();
    }
    toggleMenu(e) {
        e.preventDefault();

        const {toggleMenu} = this.props;

        toggleMenu();
    }
}

export default Header;