import React from 'react';
import Helmet from 'react-helmet'
import moment from 'moment'

class Project extends React.Component {
    render() {
        const {projectName, errors, isReady, id} = this.props;
        return <div className="content-wrapper">
            <Helmet
                title={projectName}
            />
            <section className="content-header">
                <h1>{projectName ? projectName : 'Загрузка...'}
                </h1>
                <ol className="breadcrumb">
                    <li>
                        <a href={`/projects/${id}/settings`}><i className="fa fa-cog"></i>Настройки проекта</a>
                    </li>
                </ol>
            </section>
            <section className="content">
                <div className="box">
                    <div className="box-header with-border">
                        <h3 className="box-title">Ошибки</h3>
                    </div>
                    <div className="box-body table-responsive no-padding project-box-body">
                        {!isReady ? <div className="overlay"><i className="fa fa-refresh fa-spin"></i></div> :
                            <table className="table">
                                <tbody>
                                <tr>
                                    <th>Кол-во</th>
                                    <th>Ошибка</th>
                                    <th>Тип</th>
                                    <th>Последняя дата</th>
                                </tr>
                                {errors ? errors.map(error=>
                                    <tr key={error._id}>
                                        <td>{error.count}</td>
                                        <td><a
                                            href={`/projects/${error.project}/errors/${error._id}`}>{`${error.name}: ${error.message}`}</a>
                                        </td>
                                        <td>{error.type}</td>
                                        <td>{moment(error.lastSeen).format('DD.MM.YYYY HH:mm:ss')}</td>
                                    </tr>
                                ) : isReady ? <tr className="text-center">There is no errors in this project</tr>
                                    : ""}
                                </tbody>
                            </table>}
                    </div>
                    <div className="box-footer">here</div>
                </div>
            </section>
        </div>
    }

    componentDidMount() {
        this.props.openProjectsMenu()
    }
}

export default Project;