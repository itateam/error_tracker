export default {
    login({Meteor}, email, password) {
        Meteor.loginWithPassword(email, password, function(e) {
            if (e) {
                console.error(e);
                alert('Login error: ' + e.reason);
            }
        })
    },
    logout({Meteor}) {
        Meteor.logout()
    }
}