export default {
    createProject({Meteor, FlowRouter}, name) {
        Meteor.call('projectCreate', name, function (e, res) {
            if (e) {
                alert(e.reason);
                return;
            }

            FlowRouter.go(`/projects/${res}/settings`);
        })
    },
    removeProject({Meteor, FlowRouter}, id) {
        Meteor.call('removeProject', id, function (e, res) {
            if (e) {
                alert(e.reason);
                return;
            }
            FlowRouter.go('/');
        })
    },
    saveProject({Meteor, FlowRouter}, id, name) {
        Meteor.call('saveProject', id, name, function (e, r) {
            if (e) {
                alert(e.reason);
            }
        })
    }
}