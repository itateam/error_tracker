import accounts from './accounts'
import ui from './ui'
import errors from './errors'
import projects from './projects'

export default {
    accounts,
    ui,
    errors,
    projects
};
