export default {
    toggleMenu({LocalState}) {
        const currentCollapseState = !!LocalState.get('IS_MENU_COLLAPSED');
        const currentOpenState = !!LocalState.get('IS_MENU_OPEN');
        LocalState.set('IS_MENU_COLLAPSED', !currentCollapseState);
        LocalState.set('IS_MENU_OPEN', !currentOpenState)
    },
    toggleMenuProjectsList({LocalState}) {
        const currentState = !!LocalState.get('IS_MENU_PROJECTS_LIST_OPEN');
        LocalState.set('IS_MENU_PROJECTS_LIST_OPEN', !currentState)
    },
    openProjectsMenu({LocalState}) {
        if (!LocalState.get('IS_MENU_PROJECTS_LIST_OPEN')) {
            LocalState.set('IS_MENU_PROJECTS_LIST_OPEN', true)
        }
    },
    fillProjectNameInput({LocalState}, oldName, newName) {
        LocalState.set('IS_PROJECT_NAME_DIF', oldName !== newName)
    }
}