import MainLayout from '../components/main_layout.jsx';
import {useDeps, composeWithTracker, composeAll} from 'mantra-core';
import {authComposer} from 'meteor-auth'

const composer = ({context}, onData) => {
    const {LocalState} = context();
    const isMenuCollapsed = LocalState.get('IS_MENU_COLLAPSED');
    const isMenuOpen = LocalState.get('IS_MENU_OPEN');
    onData(null, {isMenuCollapsed, isMenuOpen});
};

export default composeAll(
    composeWithTracker(authComposer),
    composeWithTracker(composer),
    useDeps()
)(MainLayout);