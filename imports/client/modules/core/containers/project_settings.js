import ProjectSettings from '../components/project_settings';
import {useDeps, composeWithTracker, composeAll} from 'mantra-core';

const composer = ({context, projectId}, onData) => {
    const {FlowRouter, Meteor, Collections, LocalState} = context();
    if (Meteor.subscribe('project', projectId).ready()) { //TODO Limit error
        if (Collections.Projects.findOne(projectId)) {
           var {name, accessHash} = Collections.Projects.findOne(projectId)
        }
        const host = Meteor.settings.public.host;
        const isSameName = !LocalState.get('IS_PROJECT_NAME_DIF');
        onData(null, {name, accessHash, host, isSameName, isReady: true, id: projectId});
    }
    else {
        onData(null, {isReady: false, id: projectId});
    }
};

const depsMaper = (context, actions) => ({
    typeProjectName: actions.ui.fillProjectNameInput,
    removeProject: actions.projects.removeProject,
    saveProject: actions.projects.saveProject,
    openProjectsMenu: actions.ui.openProjectsMenu,
    context: ()=> context
});


export default composeAll(
    composeWithTracker(composer),
    useDeps(depsMaper)
)(ProjectSettings);