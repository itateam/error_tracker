import ErrorPage from '../components/error';
import {useDeps, composeWithTracker, composeAll} from 'mantra-core';

const composer = ({context, errorId}, onData) => {
    const {FlowRouter, Meteor, Collections, LocalState} = context();
    if (Meteor.subscribe('project.error', errorId).ready()) {
        const error = Collections.Errors.findOne(errorId);
        onData(null, {error, isReady: true});
    }
    else {
        onData(null, {isReady: false});
    }
};

const depsMaper = (context, actions) => ({
    openProjectsMenu: actions.ui.openProjectsMenu,
    resolve: actions.errors.resolveError,
    context: ()=> context
});


export default composeAll(
    composeWithTracker(composer),
    useDeps(depsMaper)
)(ErrorPage);