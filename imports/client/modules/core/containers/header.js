import Header from '../components/header.jsx';
import {useDeps, composeAll} from 'mantra-core';

export const depsMapper = (context, actions) => ({
    logout: actions.accounts.logout,
    toggleMenu: actions.ui.toggleMenu,
    context: () => context
});


export default composeAll(
    useDeps(depsMapper)
)(Header);