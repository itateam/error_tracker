import Login from '../components/login.jsx';
import {useDeps, composeAll} from 'mantra-core';

export const depsMapper = (context, actions) => ({
    login: actions.accounts.login,
    context: () => context
});


export default composeAll(
    useDeps(depsMapper)
)(Login);