import SideMenu from '../components/side_menu.jsx';
import {useDeps, composeWithTracker, composeAll} from 'mantra-core';

const composer = ({context}, onData) => {
    const {FlowRouter, Meteor, Collections, LocalState} = context();
    const currentRoute = FlowRouter.current().route.name;
    const currentProject = FlowRouter.getParam('projectId');
    const isProjectsListOpen = LocalState.get('IS_MENU_PROJECTS_LIST_OPEN');
    if (Meteor.subscribe('projects.list').ready()) {
        const projects = Collections.Projects.find().fetch();
        onData(null, {currentRoute, projects, currentProject, isProjectsListOpen});
    }
    else {
        onData(null, {currentRoute, currentProject, isProjectsListOpen});
    }
};

const depsMapper = (context, actions) => ({
    toggleMenuProjectsList: actions.ui.toggleMenuProjectsList,
    context: () => context
});


export default composeAll(
    composeWithTracker(composer),
    useDeps(depsMapper)
)(SideMenu);