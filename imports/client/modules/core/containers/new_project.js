import NewProject from '../components/new_project';
import {useDeps, composeWithTracker, composeAll} from 'mantra-core';


const depsMaper = (context, actions) => ({
    createProject: actions.projects.createProject,
    context: ()=> context
});


export default composeAll(
    useDeps(depsMaper)
)(NewProject);