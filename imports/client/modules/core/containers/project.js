import Project from '../components/project';
import {useDeps, composeWithTracker, composeAll} from 'mantra-core';

const composer = ({context, projectId}, onData) => {
    const {FlowRouter, Meteor, Collections, LocalState} = context();
    if (Meteor.subscribe('project.errors', projectId).ready()) { //TODO Limit errors
        const errors = Collections.Errors.find({}, {sort: {lastSeen: -1}}).fetch();
        const projectName = Collections.Projects.findOne(projectId).name;
        onData(null, {projectName, errors, isReady: true, id: projectId});
    }
    else {
        onData(null, {isReady: false, id: projectId});
    }
};

const depsMaper = (context, actions) => ({
    openProjectsMenu: actions.ui.openProjectsMenu,
    context: ()=> context
});


export default composeAll(
    composeWithTracker(composer),
    useDeps(depsMaper)
)(Project);