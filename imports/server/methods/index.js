import {Meteor} from 'meteor/meteor';
import Collections from '/imports/common/collections'
import {_} from 'meteor/underscore'
import sha1 from 'crypto-js/sha1'

//TODO rewrite methods via es6
export default function () {

    Meteor.methods({
        addError: function(error) {
            this.unblock();
            var message, modifier, normalizedError;
            normalizedError = {
                project: Collections.Projects.findOne({accessHash: error.accessHash})._id,
                message: error.message.replace(/["']/g, "").replace("is undefined", "is not defined").replace("не определено", "is not defined"),
                stack: JSON.stringify(error.stack),
                type: 'js',
                env: error.env,
                name: error.name || "unknown error",
                page: error.page,
                url: error.url,
                state: 'unresolved'
            };
            message = `${normalizedError.message}`;
            _.first(error.stack, function(frame) {
                return message +=  frame.url + frame.line;
            });
            normalizedError.fingerprint = sha1(message).toString();
            if (error.userAgent) {
                normalizedError.userAgent = error.userAgent;
            }
            if (error.screen) {
                normalizedError.resolution = JSON.stringify(error.screen);
            }
            if (Collections.Errors.find({
                    project: normalizedError.project,
                    fingerprint: normalizedError.fingerprint,
                    env: normalizedError.env
                }).count()) {
                modifier = _.omit(normalizedError, ['project', 'message', 'fingerprint', 'env', 'name']);
                return Collections.Errors.update({
                    project: normalizedError.project,
                    fingerprint: normalizedError.fingerprint,
                    env: normalizedError.env
                }, {
                    $set: modifier
                });
            } else {
                return Collections.Errors.insert(normalizedError);
            }
        },
        projectCreate(name) {
            //TODO Name validation (unique?)
            return Collections.Projects.insert({name: name});
        }
    });
}