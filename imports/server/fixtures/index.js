import {Meteor} from 'meteor/meteor';
import Collections from '/imports/common/collections';
import {Accounts} from 'meteor/accounts-base'

export default function () {
    if (Meteor.users.find().count() == 0) {
        const {adminEmail, adminPwd} = Meteor.settings.load;
        Accounts.createUser({email: adminEmail, password: adminPwd})
    }
}