import {Meteor} from 'meteor/meteor'
import Collections from '/imports/common/collections'

export default function () {
    Meteor.publish('projects.list', function () {
        return Collections.Projects.find(); //TODO filter by userId
    });
    Meteor.publish('project', function (id) {
        check(id, String);
        return Collections.Projects.find(id);
    });
    Meteor.publish('project.errors', function (projectId) {
        check(projectId, String);
        
        return [
            Collections.Projects.find(projectId),
            Collections.Errors.find({project: projectId})
        ]
    })
    Meteor.publish('project.error', function (errorId) {
        check(errorId, String);

        return [
            Collections.Errors.find(errorId)
        ]
    })
}
