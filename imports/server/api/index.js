import {Picker} from 'meteor/meteorhacks:picker';
import methods from '/imports/server/methods';
import {Errors, Projects} from '/imports/common/collections';

//npm imports
import corser from 'corser';
import parser from 'body-parser';


export default function () {
    methods();
    
    Picker.middleware(corser.create({methods: ['POST']}));
    Picker.middleware(parser.json());
    Picker.middleware(parser.urlencoded( { extended: false } ));

    Picker.route('/report', function (params, req, res, next) {
        //TODO do some checks

        const result = Meteor.call('addError', req.body);

        res.setHeader( 'Content-Type', 'application/json' );
        res.statusCode = 200;
        res.end(JSON.stringify(result));
    })
}