import {Meteor} from 'meteor/meteor';
import Collections from '/imports/common/collections'

export default function () {
    Meteor.methods({
        resolveError(errorId) {
            //TODO some checks
            Collections.Errors.update(errorId, {$set: {state: 'resolved'}});
        },
        removeProject(id) {
            //TODO perform soft delete?
            Collections.Errors.remove({project: id});
            Collections.Projects.remove(id);
        },
        saveProject(id, name) {
            Collections.Projects.update(id, {$set: {"name": name}})
        }
    })
}