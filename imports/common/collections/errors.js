import {Mongo} from 'meteor/mongo';
import {SimpleSchema} from 'meteor/aldeed:simple-schema'
import parser from 'ua-parser-js'

const Errors = new Mongo.Collection('errors');

const schema = new SimpleSchema({
    count: {
        type: Number,
        autoValue: function () {
            if (Meteor.isServer) {
                if (this.isInsert) {
                    return 1;
                } else if (this.isUpdate) {
                    var state = this.field('state');
                    if (state.value != 'resolved') {
                        return {
                            $inc: 1
                        };
                    }
                    else {
                        return this.unset()
                    }

                } else {
                    return this.unset();
                }
            } else {
                return this.unset();
            }
        }
    },
    state: {
        type: String,
        label: 'Error state',
        allowedValues: ['resolved', 'unresolved'],
        defaultValue: 'unresolved'
    },
    name: {
        type: String,
        label: 'Error name',
        autoValue: function () {
            if (Meteor.isServer) {
                if (this.isUpdate) {
                    return this.unset();
                }
            } else {
                return this.unset();
            }
        }
    },
    fingerprint: {
        type: String,
        label: 'Error fingerprint',
        autoValue: function () {
            if (Meteor.isServer) {
                if (this.isUpdate) {
                    return this.unset();
                }
            } else {
                return this.unset();
            }
        }
    },
    stack: {
        type: [Object],
        label: 'Stacktrace',
        blackbox: true,
        autoValue: function () {
            if (Meteor.isServer) {
                if (this.isSet) {
                    if (this.isInsert) {
                        return JSON.parse(this.value);
                    } else if (this.isUpdate) {
                        return this.unset();
                    }
                }
            } else {
                return this.unset();
            }
        },
        optional: true
    },
    project: {
        type: String,
        label: 'Project',
        autoValue: function () {
            if (Meteor.isServer) {
                if (this.isSet) {
                    if (this.isUpdate) {
                        return this.unset();
                    }
                }
            } else {
                return this.unset();
            }
        }
    },
    message: {
        type: String,
        label: 'Error message',
        autoValue: function () {
            if (Meteor.isServer) {
                if (this.isSet) {
                    if (this.isUpdate) {
                        return this.unset();
                    }
                }
            } else {
                return this.unset();
            }
        }
    },
    page: {
        type: String,
        label: 'Error page url',
        optional: true
    },
    url: {
        type: String,
        label: 'Error file url',
        optional: true
    },
    dates: {
        type: [Date],
        label: 'Error date',
        autoValue: function () {
            if (Meteor.isServer) {
                if (this.isInsert) {
                    return [new Date()];
                } else if (this.isUpdate) {
                    var state = this.field('state');
                    if (state.value != 'resolved') {
                        return {
                            $push: new Date()
                        };
                    }
                    else {
                        return this.unset()
                    }
                } else {
                    return this.unset();
                }

            } else {
                return this.unset();
            }
        }
    },
    lastSeen: {
        type: Date,
        label: 'Last seen this error',
        autoValue: function () {
            if (this.isUpdate) {
                var state = this.field('state');
                if (state.value == 'resolved') {
                    return this.unset()
                }
            }
            return new Date();
        }
    },
    type: {
        type: String,
        label: 'Error type',
        allowedValues: ['js']
    },
    env: {
        type: String,
        label: 'Client environment'
    },
    userAgent: {
        type: [String],
        label: 'Client useragent',
        optional: true,
        autoValue: function () {
            if (Meteor.isServer) {
                if (this.isSet) {
                    if (this.isInsert) {
                        return [this.value];
                    } else if (this.isUpdate) {
                        return {
                            $addToSet: this.value
                        };
                    } else {
                        return this.unset();
                    }
                }
            } else {
                return this.unset();
            }
        }
    },
    browser: {
        type: [Object],
        label: 'Error browser',
        optional: true,
        autoValue: function () {
            var ua, val;
            if (Meteor.isServer) {
                ua = this.field('userAgent');
                if (ua.isSet) {
                    val = parser(ua.value);
                    if (this.isInsert) {
                        return [
                            {
                                name: val.browser.name,
                                version: val.browser.major
                            }
                        ];
                    } else if (this.isUpdate) {
                        return {
                            $addToSet: {
                                name: val.browser.name,
                                version: val.browser.major
                            }
                        };
                    } else {
                        return this.unset();
                    }
                }
            } else {
                return this.unset();
            }
        }
    },
    'browser.$.name': {
        type: String,
        label: 'Browser name',
        optional: true
    },
    'browser.$.version': {
        type: String,
        label: 'Browser version',
        optional: true
    },
    resolution: {
        type: [Object],
        label: 'Screen dimentions',
        optional: true,
        autoValue: function () {
            if (Meteor.isServer) {
                if (this.isSet) {
                    if (this.isInsert) {
                        return [JSON.parse(this.value)];
                    } else if (this.isUpdate) {
                        return {
                            $addToSet: JSON.parse(this.value)
                        };
                    } else {
                        return this.unset();
                    }
                }
            } else {
                return this.unset();
            }
        }
    },
    'resolution.$.width': {
        type: Number,
        label: 'Screen width',
        optional: true
    },
    'resolution.$.height': {
        type: Number,
        label: 'Screen height',
        optional: true
    },
    os: {
        type: [String],
        label: 'Os',
        optional: true,
        autoValue: function () {
            var ua, val;
            if (Meteor.isServer) {
                ua = this.field('userAgent');
                if (ua.isSet) {
                    val = parser(ua.value);
                    if (this.isInsert) {
                        return [val.os.name];
                    } else if (this.isUpdate) {
                        return {
                            $addToSet: val.os.name
                        };
                    } else {
                        return this.unset();
                    }
                }
            } else {
                return this.unset();
            }
        }
    }
});

Errors.attachSchema(schema);

export default Errors;