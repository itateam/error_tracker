import Errors from './errors'
import Projects from './projects'

export default {
    Errors,
    Projects
};