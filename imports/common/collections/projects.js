import {Mongo} from 'meteor/mongo';
import {SimpleSchema} from 'meteor/aldeed:simple-schema'
import {Random} from 'meteor/random'
import {Meteor} from 'meteor/meteor'

const Projects = new Mongo.Collection('projects');

const schema = new SimpleSchema({
    accessHash: {
        type: String,
        label: 'Access Hash',
        autoValue: function() {
            if (this.isInsert) {
                return Random.id();
            } else if (this.isUpsert) {
                return {
                    $setOnInsert: Random.id()
                };
            } else {
                return this.unset();
            }
        }
    },
    name: {
        type: String,
        label: 'Project name',
        max: 100
    },
    users: {
        type: [String],
        label: 'Project users list',
        optional: true //TODO Remove
    }
});

Projects.attachSchema(schema);

Projects.allow({
    insert: function(userId, doc) {
        return !!userId && Meteor.isServer;
    }
});

export default Projects;