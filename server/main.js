import pubs from '/imports/server/pubs';
import api from '/imports/server/api';
import fixtures from '/imports/server/fixtures'
import methods from '/imports/common/methods'


pubs();
fixtures();
api();
methods();
